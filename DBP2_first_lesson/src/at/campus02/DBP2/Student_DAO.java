package at.campus02.DBP2;

import java.util.List;

public interface Student_DAO {

    void create(Student student);
    Student read(Integer rollNumber);
    void update(Student student);
    void delete(Student student);

    List<Student> getAllStudents();
}
