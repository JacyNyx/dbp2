package at.campus02.DBP2;

public class Application_Student {

    public static void main(String[] args) {

        //Student_DAO dao = new DAO_MemoryImpl();
        Student_DAO dao =
                new DB_Impl(
                "jdbc:derby:database;create=true");

        //CREATE
        Student first = new Student();
        first.setRollNumber(100);
        first.setFirstName("Martin");
        first.setLastName("Kainz");
        dao.create(first);

        Student secound = new Student();
        secound.setRollNumber(101);
        secound.setFirstName("Maria");
        secound.setLastName("Rainer");
        dao.create(secound);

        Student third = new Student();
        third.setRollNumber(102);
        third.setFirstName("Tatjana");
        third.setLastName("Kallbring");
        dao.create(third);

        //READ ALL
        System.out.println(dao.getAllStudents());

        //READ
        Student firstFromSTorage = dao.read(100);
        System.out.println("READ: " + firstFromSTorage);

        //UPDATE
        Student updated = new Student();
        updated.setRollNumber(first.getRollNumber());
        updated.setFirstName(first.getFirstName());
        updated.setLastName("Kielmann");
        dao.update(updated);

        System.out.println("UPDATE: " + dao.read(updated.getRollNumber()));

        //DELETE

        dao.delete(secound);
        System.out.println("DELETE: " + dao.getAllStudents());
    }
}
