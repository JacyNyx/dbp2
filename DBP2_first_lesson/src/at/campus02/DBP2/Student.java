package at.campus02.DBP2;

public class Student {
    private Integer rollNumber;
    private String firstName;
    private String lastName;

    public Integer getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(Integer rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("rollNumber: ").append(rollNumber);
        sb.append(", firstName: ").append(firstName).append('\'');
        sb.append(", lastName: ").append(lastName).append('\n');
        return sb.toString();
    }
}
