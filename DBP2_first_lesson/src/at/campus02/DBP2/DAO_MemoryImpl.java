package at.campus02.DBP2;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.ArrayList;
import java.util.List;

public class DAO_MemoryImpl implements Student_DAO {

    private List<Student> students = new ArrayList<>();

    @Override
    public void create(Student student) {
    students.add(student);
    }

    @Override
    public Student read(Integer rollNumber) {
        if(rollNumber == null){
            return null;
        }
        for (Student student : students) {
            if(rollNumber.equals(student.getRollNumber())){
                return student;
            }
        }
        return null;
    }

    @Override
    public void update(Student student) {
        if(student == null){
            return;
        }
        Student stored = read(student.getRollNumber());
        if(stored != null){
            // Variante 1 (direkte änderung am Objekt)
            stored.setFirstName(student.getFirstName());
            stored.setLastName(student.getLastName());

            // Variante 2 (erstetzt das Objekt direkt in der Liste)
            students.remove(stored);
            students.add(student);
        }
    }

    @Override
    public void delete(Student student) {
        if(student == null){
            return;}
        Student stored = read(student.getRollNumber());
        if (stored != null){
            students.remove(stored);
        }

        }


    @Override
    public List<Student> getAllStudents() {
      return students;
    }
}
