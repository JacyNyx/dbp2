package at.campus02.DBP2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.omg.CORBA.ORB.init;

public class DB_Impl implements Student_DAO{

    private String connectionURL;
    private Connection connection;

    public DB_Impl(String connectionURL) {
        this.connectionURL = connectionURL;
        doInitConnection();
        if(!ensureStudentsTable()){
            throw new IllegalStateException("couldn´t create Table");
        }
    }

    private boolean ensureStudentsTable(){
        return true;
    }

    private void doInitConnection() {
        String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        try {
            Class.forName(driver).newInstance();
            connection = DriverManager.getConnection(connectionURL);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
           throw new IllegalStateException(e);
        }
    }

    @Override
    public void create(Student student) {

    }

    @Override
    public Student read(Integer rollNumber) {
        return null;
    }

    @Override
    public void update(Student student) {

    }

    @Override
    public void delete(Student student) {

    }

    @Override
    public List<Student> getAllStudents() {
        return null;
    }
}
